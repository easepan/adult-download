package com.github.zjiajun.adult.scheduler;

import com.github.zjiajun.adult.request.Request;
import com.github.zjiajun.adult.response.Response;

/**
 * @author zhujiajun
 * @since 2017/4/5
 */
public interface Scheduler {

	/**
	 * put request
	 * 
	 * @param request
	 */
	void putRequest(Request request);

	/**
	 * take request
	 * 
	 * @return
	 */
	Request takeRequest();

	/**
	 * put response
	 * 
	 * @param response
	 */
	void putResponse(Response response);

	/**
	 * take response
	 * 
	 * @return
	 */
	Response takeResponse();

}

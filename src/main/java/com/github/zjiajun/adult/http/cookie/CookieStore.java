package com.github.zjiajun.adult.http.cookie;

import okhttp3.Cookie;
import okhttp3.HttpUrl;

import java.util.List;

/**
 * @author zhujiajun
 * @since 2017/2/3
 */
public interface CookieStore {

	/**
	 * store cookie
	 * 
	 * @param httpUrl
	 * @param cookieList
	 */
	void store(HttpUrl httpUrl, List<Cookie> cookieList);

	/**
	 * lookup cookie
	 * 
	 * @param httpUrl
	 * @return
	 */
	List<Cookie> lookup(HttpUrl httpUrl);
}

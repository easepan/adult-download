package com.github.zjiajun.adult.request;

import java.util.Map;

/**
 * @author zhujiajun
 * @since 2017/4/14
 */
public interface LoginParamBuild {

	/**
	 * empty login information
	 * 
	 * @param emptyLoginParam
	 */
	void param(Map<String, String> emptyLoginParam);

}

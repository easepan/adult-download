package com.github.zjiajun.adult.request;

/**
 * @author zhujiajun
 * @since 2017/4/6
 */
public enum Method {

	/**
	 * 使用方法
	 */
	GET, POST;

	Method() {
	}
}
